import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/httpservice.service';
import {recipemodel} from '../recipemodel.model';
@Component({
  selector: 'app-recipeslist',
  templateUrl: './recipeslist.component.html',
  styleUrls: ['./recipeslist.component.css']
})
export class RecipeslistComponent implements OnInit {
  // recipes : recipemodel[] = [
  //   new recipemodel('testname', 'testdescription', 'http://www.seriouseats.com/recipes/assets_c/2016/05/20160503-fava-carrot-ricotta-salad-recipe-1-thumb-1500xauto-431710.jpg')
  //  ];
  recipeitems: recipemodel[];
  error: any;
  constructor(private recipeService: HttpService) { }

  ngOnInit() {
    this.loadrecipelist();
  };

  loadrecipelist() {
    let jsonurl = 'http://www.json-generator.com/api/json/get/clKoJfVovC';
    this.recipeService.getUser(jsonurl).subscribe(data => this.recipeitems = data, error => this.error = error);
  };
}
