import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {TestService} from '../services/testservice.service'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
headel = 'from header.....';
newname = 'new name...';
@ViewChild('ti1') ti2:ElementRef;

  constructor(private TestService:TestService) { }

  ngOnInit() {
  }
  showinput() {  
    console.log(this.ti2);
  }
  takeinput(ivalue) {
    console.log('ivalue,,,', ivalue);
  }
  getdnamenew() {    
    this.newname = this.TestService.getdname() ;    
  }
}
