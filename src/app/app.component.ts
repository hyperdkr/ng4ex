import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from './services/httpservice.service';
import { HttppromiseService } from './services/httppromise.service';
import { TestService } from './services/testservice.service';
import { HeaderComponent } from './header/header.component';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'app';
    profile: any;
    error: any;
    dname: string = 'Dhiya Ramal';
    songdetails: Object;
    @ViewChild(HeaderComponent) hc: HeaderComponent;

    constructor(private userService: HttpService, private testservice: TestService, private ituneservice: HttppromiseService) { }
    ngOnInit(): void {

        this.loadUser();

    }

    loadUser() {
        let jsonurl = 'http://www.json-generator.com/api/json/get/bTPgALPqnC?indent=2';
        this.userService.getUser(jsonurl).subscribe(data => this.profile = data, error => this.error = error);
    }
    passtoservice() {
        this.testservice.setdname(this.dname);
    }
    searchsong(searchterm: string) {
        console.log('searchterm...', searchterm);
        this.ituneservice.searchitune(searchterm).subscribe(arg => this.songdetails = arg);
        console.log(this.songdetails);
    }
}

