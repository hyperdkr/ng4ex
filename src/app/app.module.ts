import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpService } from './services/httpservice.service';
import { TestService } from './services/testservice.service';
import { HttppromiseService } from './services/httppromise.service';
import { AppComponent } from './app.component';
import { RecipesComponent } from './recipes/recipes.component';
import { ShoppinglistComponent } from './shoppinglist/shoppinglist.component';
import { ShoppinglisteditComponent } from './shoppinglist/shoppinglistedit/shoppinglistedit.component';
import { RecipeslistComponent } from './recipes/recipeslist/recipeslist.component';
import { RecipesdetailComponent } from './recipes/recipesdetail/recipesdetail.component';
import { HeaderComponent } from './header/header.component';
import { RecipeitemComponent } from './recipes/recipeslist/recipeitem/recipeitem.component';
import { RegisterComponent } from './customer/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    RecipesComponent,
    ShoppinglistComponent,
    ShoppinglisteditComponent,
    RecipeslistComponent,
    RecipesdetailComponent,
    HeaderComponent,
    RecipeitemComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule, HttpModule, ReactiveFormsModule, FormsModule
  ],
  providers: [HttpService, TestService, HttppromiseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
